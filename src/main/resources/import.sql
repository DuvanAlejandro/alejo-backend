INSERT INTO hire_sectors (id,sector_name) VALUES (1,'sector 1');
INSERT INTO hire_sectors (id,sector_name) VALUES (2,'sector 2');
INSERT INTO hire_sectors (id,sector_name) VALUES (3,'sector 3');
INSERT INTO hire_sectors (id,sector_name) VALUES (4,'sector 4');

INSERT INTO hire_user (id,address,email,last_name,name,pass) VALUES (1,'calle 6','alejo64@hotmaail.com','Rodriguez','Fabian','1234');
INSERT INTO hire_user (id,address,email,last_name,name,pass) VALUES (2,'calle 7','gabriel@hotmaail.com','Rodriguez','Fabian','1234');
INSERT INTO hire_user (id,address,email,last_name,name,pass) VALUES (3,'calle 8','ana@hotmaail.com','Rodriguez','Fabian','1234');
INSERT INTO hire_user (id,address,email,last_name,name,pass) VALUES (4,'calle 9','prueba@hotmail.com','Rodriguez','Fabian','1234');

INSERT INTO hire_clients (id,address,city,email,name,nit,sector_id,user_id) VALUES (11,'calle 88','Bogota','alejo64@hotmaail.com','Andres','b',1,1);
INSERT INTO hire_clients (id,address,city,email,name,nit,sector_id,user_id) VALUES (21,'calle 99','Bogota','gabriel@hotmaail.com','Andres','b',1,1);
INSERT INTO hire_clients (id,address,city,email,name,nit,sector_id,user_id) VALUES (31,'calle 33','Bogota','ana@hotmaail.com','b','Andres',2,2);
INSERT INTO hire_clients (id,address,city,email,name,nit,sector_id,user_id) VALUES (41,'calle 66','Bogota','prueba@hotmail.com','b','Andres',2,2);
