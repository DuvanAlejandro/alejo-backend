package com.bofugroup.springboot.backend.apirest.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.bofugroup.springboot.backend.apirest.models.entity.Client;



public interface IClienteDAO extends CrudRepository<Client, Long>{
	
	
	@Query(value = "SELECT * FROM hire_clients WHERE user_id = ?1",nativeQuery = true)
	public List<Client> findAllByUser(Long user_id);
	
	//public List<Client> findAllByUser_id(Long user_id);

}
