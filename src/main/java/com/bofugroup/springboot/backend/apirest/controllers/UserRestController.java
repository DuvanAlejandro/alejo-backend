package com.bofugroup.springboot.backend.apirest.controllers;


import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bofugroup.springboot.backend.apirest.models.dto.DTOLogin;
import com.bofugroup.springboot.backend.apirest.models.dto.DTOUser;
import com.bofugroup.springboot.backend.apirest.models.services.UserService;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = {"http://localhost:4200"})
public class UserRestController {
	
	@Autowired
	private UserService userService;
	
	//Obtiene todos los clientes de respectivo usuario
	@GetMapping("/user/{id}")
	public ResponseEntity<?> findById(@PathVariable Long id) {
		
		Map<String, Object> response = new HashMap<>();
		
		
		DTOUser usuario = null;
		try {
			
			usuario=userService.findById(id);
				
		}catch(DataAccessException e) {
			
			response.put("message", "Request error in DB");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
		}
		
		if(usuario==null) {
			response.put("message","Object is null");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		response.put("message", "Successfull request Client ");
		response.put("usuario", usuario);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
	}
	
	//Obtiene el usuario que esta intentando entrar a su cuenta
	@PostMapping("/user/log")
	public ResponseEntity<?> login(@RequestBody DTOLogin login) {
		
		DTOUser userLog =null;
		
		Map<String, Object> response = new HashMap<>();
		
		
		if(login==null) {
			response.put("message","Object is null");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			
			userLog = userService.Login(login);
			
		}catch(DataAccessException e) {
			
			response.put("message","DB Conflict");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.CONFLICT);
			
		}
		
		if(userLog==null) {
			response.put("message","User don´t exist");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.BAD_REQUEST);
			
		}else if(!login.getPassword().equals(userLog.getPass())){
			 
				
				System.out.print("log: "+login.getPassword()+" log2: "+userLog.getPass());
				response.put("message","Incorrect Password");
				return new ResponseEntity<Map<String, Object>>(response,HttpStatus.BAD_REQUEST);
			
		}
		
		response.put("message", "Successfull Login");
		response.put("usuario", userLog);  
		
		 return new ResponseEntity<Map<String, Object>>(response,HttpStatus.ACCEPTED);
	
	}
	
	
	@PostMapping("/user")
	public ResponseEntity<?> create(@RequestBody DTOUser user) {
		
		DTOUser userNew=null;
		
		Map<String, Object> response = new HashMap<>();
		
		
		if(user==null) {
			response.put("message","Object is null");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		
		try {
			
			userNew = userService.create(user);
			
		}catch(DataAccessException e) {
			
			response.put("message","DB Conflict");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.CONFLICT);
			
		}
		
		response.put("message", "Successfull Client Create");
		response.put("usuario", userNew);
		
		 return new ResponseEntity<Map<String, Object>>(response,HttpStatus.CREATED);
	}
	
	@DeleteMapping("/user/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		
		Map<String, Object> response = new HashMap<>();
		
		if(userService.findById(id)==null) {
			response.put("message","Object is null");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			
			userService.delete(id);
				
		}catch(DataAccessException e) {
			
			response.put("message", "Delete error in DB");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
		}
		
		response.put("message", "Successfull Client delete");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
	}
	
	@PutMapping("/user/{id}")
	public ResponseEntity<?> update(@RequestBody DTOUser user, @PathVariable Long id ) {
		
		//DTOUser clienteActual = userService.findById(id);
		Map<String, Object> response = new HashMap<>();
		DTOUser clienteUpload =  null;
		
		if(userService.findById(id) == null) {
			response.put("mensaje", "Client not Found in DB");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			clienteUpload = userService.update(id, user);
			
		}catch(DataAccessException e) {
			
			response.put("mensaje", "Update error");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
			
		}
		response.put("mensaje", "Successfull Client Update");
		response.put("usuario", clienteUpload);
		
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
		
	}
	
	
	

}
