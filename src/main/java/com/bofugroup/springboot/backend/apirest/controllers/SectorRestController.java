package com.bofugroup.springboot.backend.apirest.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bofugroup.springboot.backend.apirest.models.dto.DTOSector;
import com.bofugroup.springboot.backend.apirest.models.services.SectorService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = {"http://localhost:4200"})
public class SectorRestController {
	
	@Autowired
	private SectorService sectorService;
	
    //Obtiene la informacion de toso los Sectores
	@GetMapping("/sector")
	public ResponseEntity<?> getAll(){
		
		
		Map<String, Object> response = new HashMap<>();
		List<DTOSector> sectores = null;
		
		try {
			
			sectores = sectorService.findAll();
				
		}catch(DataAccessException e) {
			
			response.put("message", "Request error in DB");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
		}
		
		if(sectores==null) {
			response.put("message","Object is null");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		response.put("message", "Successfull request Client ");
		response.put("sectores", sectores);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
	}
	
	//Obtiene la informacion de un sector especifico
	
	@GetMapping("/sector/name")
	public ResponseEntity<?> getByName(@RequestBody String name){
		
		
		Map<String, Object> response = new HashMap<>();
		DTOSector sector = null;
		
		try {
			
			sector = sectorService.findByName(name);
				
		}catch(DataAccessException e) {
			
			response.put("message", "Request error in DB");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
		}
		
		if(sector==null) {
			response.put("message","Object is null");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		response.put("message", "Successfull Request Client ");
		response.put("sector", sector);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
	}

}
