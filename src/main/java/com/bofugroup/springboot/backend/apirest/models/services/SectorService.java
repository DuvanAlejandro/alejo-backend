package com.bofugroup.springboot.backend.apirest.models.services;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bofugroup.springboot.backend.apirest.models.dao.ISectorDAO;
import com.bofugroup.springboot.backend.apirest.models.dto.DTOSector;
import com.bofugroup.springboot.backend.apirest.models.entity.Sector;

@Service
public class SectorService {
	
	@Autowired
	private ISectorDAO sectorRepo;
	
	@Autowired
	ModelMapper modelMapper;
	
	
	public List<DTOSector> findAll() {
		// TODO Auto-generated method stub
		
		List<Sector> sectores = (List<Sector>) sectorRepo.findAll();
		
		return mapearLista(sectores);

	}
	
	public List<DTOSector> mapearLista(List<Sector> sectores) {
		
		List<DTOSector> nuevaLista=new ArrayList<>();
		
		for(int i=0;i<sectores.size();i++) {
			
			
			nuevaLista.add(modelMapper.map(sectores.get(i), DTOSector.class));
		}
		
		return nuevaLista;
		
	}
	
	public DTOSector findByName(String name) {
		// TODO Auto-generated method stub
		
		 Sector sector = sectorRepo.findAllByNameSector(name);
		
		return modelMapper.map(sector,DTOSector.class);

	}

}
