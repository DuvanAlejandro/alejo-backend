package com.bofugroup.springboot.backend.apirest.models.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bofugroup.springboot.backend.apirest.models.dao.IClienteDAO;
import com.bofugroup.springboot.backend.apirest.models.dao.IUserDAO;
import com.bofugroup.springboot.backend.apirest.models.dto.DTOClient;
import com.bofugroup.springboot.backend.apirest.models.dto.DTOUser;
import com.bofugroup.springboot.backend.apirest.models.entity.Client;
import com.bofugroup.springboot.backend.apirest.models.entity.Sector;
import com.bofugroup.springboot.backend.apirest.models.entity.User;
import java.lang.reflect.Type;


@Service
public class ClienteService {

	
	@Autowired
	private IClienteDAO clientRepo;
	
	
	@Autowired
	ModelMapper modelMapper;
	
	

	@Transactional
	public List<DTOClient> findAll(Long idUser) {
		// TODO Auto-generated method stub
		List<Client> clientes = clientRepo.findAllByUser(idUser);
	
		return mapearLista(clientes);

	}
	
	public List<DTOClient> mapearLista(List<Client> clientes) {
		
		List<DTOClient> nuevaLista=new ArrayList<>();
		
		for(int i=0;i<clientes.size();i++) {
			
			
			nuevaLista.add(modelMapper.map(clientes.get(i), DTOClient.class));
		}
		
		return nuevaLista;
		
	}

	
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		clientRepo.deleteById(id);
		
	}


	@Transactional
	public DTOClient save(DTOClient cliente) {
		// TODO Auto-generated method stub
		
		
		Client nuevo = modelMapper.map(cliente, Client.class);
		Client userMap = clientRepo.save(nuevo);
		
		DTOClient dtoclient =  modelMapper.map(userMap, DTOClient.class);
		return dtoclient;
		
		
	}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 


	@Transactional
	public Client findById(Long id) {
		// TODO Auto-generated method stub
		return clientRepo.findById(id).orElse(null);
	}
	
	
	
	

}
