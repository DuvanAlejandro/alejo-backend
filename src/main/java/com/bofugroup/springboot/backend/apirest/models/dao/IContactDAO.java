package com.bofugroup.springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bofugroup.springboot.backend.apirest.models.entity.Contact;

public interface IContactDAO extends CrudRepository<Contact,Long>{

}
