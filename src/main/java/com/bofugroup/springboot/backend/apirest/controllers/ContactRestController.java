package com.bofugroup.springboot.backend.apirest.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bofugroup.springboot.backend.apirest.models.dto.DTOClient;
import com.bofugroup.springboot.backend.apirest.models.dto.DTOContact;
import com.bofugroup.springboot.backend.apirest.models.services.ClienteService;
import com.bofugroup.springboot.backend.apirest.models.services.ContactService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = {"http://localhost:4200"})
public class ContactRestController {
	
	@Autowired
	private ContactService contactService;
	
	
	//Crea un contacto nuevo
	@PostMapping("/contact")
	public ResponseEntity<?> create(@RequestBody DTOContact contact) {
		
		DTOContact contactnew=null;
		
		Map<String, Object> response = new HashMap<>();
		
		if(contact==null) {
			response.put("message","Object is null");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			
			contactnew = contactService.save(contact);
			
		}catch(DataAccessException e) {
			
			response.put("mensaje","Insertion Error");
			
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		response.put("mensaje", "Successfull Create");
		response.put("contacto", contactnew);
		
		 return new ResponseEntity<Map<String, Object>>(response,HttpStatus.CREATED);
	}

}
