package com.bofugroup.springboot.backend.apirest.models.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bofugroup.springboot.backend.apirest.models.dao.IContactDAO;
import com.bofugroup.springboot.backend.apirest.models.dao.IUserDAO;
import com.bofugroup.springboot.backend.apirest.models.dto.DTOContact;
import com.bofugroup.springboot.backend.apirest.models.entity.Contact;


@Service
public class ContactService {
	
	@Autowired
	private IContactDAO contactRepo;
	
	@Autowired
	ModelMapper modelMapper;
	
	
	public DTOContact save(DTOContact contact) {
		
		Contact newcont = modelMapper.map(contact,Contact.class);
		
		contactRepo.save(newcont);
		
		return modelMapper.map(newcont,DTOContact.class);
		
	}

}
