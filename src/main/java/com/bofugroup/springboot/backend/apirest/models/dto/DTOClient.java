package com.bofugroup.springboot.backend.apirest.models.dto;

import java.util.Date;

import com.bofugroup.springboot.backend.apirest.models.entity.Sector;
import com.bofugroup.springboot.backend.apirest.models.entity.User;


public class DTOClient {
	
private Long id;
	
	private String name;
	
	private String email;
	
	private String nit;
	
	private String address;
	
	private String city;
	
	private Date created_date;
	
	User usuario;
	Sector sector;
	
	public User getUsuario() {
		return usuario;
	}


	public void setUsuario(User usuario) {
		this.usuario = usuario;
	}


	public Sector getSector() {
		return sector;
	}


	public void setSector(Sector sector) {
		this.sector = sector;
	}
	
	
	public void prePersist() {
		created_date = new Date();
		
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getNit() {
		return nit;
	}


	public void setNit(String nit) {
		this.nit = nit;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public Date getCreate_date() {
		return created_date;
	}


	public void setCreate_date(Date create_date) {
		this.created_date = create_date;
	}




}
