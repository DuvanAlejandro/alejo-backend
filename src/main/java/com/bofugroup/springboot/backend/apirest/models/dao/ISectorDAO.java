package com.bofugroup.springboot.backend.apirest.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bofugroup.springboot.backend.apirest.models.entity.Client;
import com.bofugroup.springboot.backend.apirest.models.entity.Sector;

public interface ISectorDAO extends CrudRepository<Sector,Long>{
	
	@Query(value = "SELECT * FROM hire_sectors WHERE sector_name = ?1",nativeQuery = true)
	public Sector findAllByNameSector(String sector_name);

}
