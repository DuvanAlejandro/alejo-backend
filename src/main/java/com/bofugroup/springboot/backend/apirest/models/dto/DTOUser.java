package com.bofugroup.springboot.backend.apirest.models.dto;

import java.util.Date;
import java.util.List;

import com.bofugroup.springboot.backend.apirest.models.entity.Client;


public class DTOUser {
	
	
	private Long id;
	private String name;
	private String last_name;
	private String address;
	private String email;
	private String pass;
	private Date last_login;
	

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail() {
		return email;
	}
	
	

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Date getLast_lgin() {
		return last_login;
	}

	public void setLast_lgin(Date last_login) {
		this.last_login = last_login;
	}

	
	

}
