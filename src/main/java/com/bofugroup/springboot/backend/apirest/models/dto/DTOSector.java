package com.bofugroup.springboot.backend.apirest.models.dto;

public class DTOSector {
	
	private Long id;
	private String sector_name;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSector_name() {
		return sector_name;
	}
	public void setSector_name(String sector_name) {
		this.sector_name = sector_name;
	}
	

}
