package com.bofugroup.springboot.backend.apirest.models.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bofugroup.springboot.backend.apirest.models.dao.IUserDAO;
import com.bofugroup.springboot.backend.apirest.models.dto.DTOLogin;
import com.bofugroup.springboot.backend.apirest.models.dto.DTOUser;
import com.bofugroup.springboot.backend.apirest.models.entity.Client;
import com.bofugroup.springboot.backend.apirest.models.entity.User;

@Service
public class UserService {
	
	
	@Autowired
	IUserDAO userRepo;
	
	@Autowired
	ModelMapper modelMapper;
	
	
	public DTOUser create(DTOUser usuario) {
		
		User nuevo = modelMapper.map(usuario, User.class);
		User userMap = userRepo.save(nuevo);
		DTOUser dtouser =  modelMapper.map(userMap, DTOUser.class);
		return dtouser;
		
	}

	public DTOUser update(Long id , DTOUser newUser) {
		
		 return userRepo.findById(id).map(user ->{
			
			
			user.setName(newUser.getName());
			user.setLast_name(newUser.getLast_name());
			user.setLast_lgin(newUser.getLast_lgin());
			user.setEmail(newUser.getEmail());
			user.setPass(newUser.getPass());
			
			User nu = userRepo.save(user);
			
			DTOUser notice = modelMapper.map(nu, DTOUser.class);
			return notice;
			
			
		}).orElse(null);
		
	}
	
	
	public void delete(Long id) {
		
		userRepo.findById(id).map(user ->{
			
			 userRepo.delete(user);
			 return ResponseEntity.ok().build();
			 
		});
	}
	
	public DTOUser findById(Long id) {
		// TODO Auto-generated method stub
		
		User u = userRepo.findById(id).orElse(null);
		DTOUser user=null;
		if(u!=null) {
			user = modelMapper.map(u, DTOUser.class);
		}else {
			return null;
		}
		
		
		return  user;
		
		
	}
	
	public DTOUser Login(DTOLogin login) {
		
		
		List<User> user= userRepo.findByEmail(login.getUsername());
		DTOUser usuario=null;
		
		if(user!=null) {
			usuario = modelMapper.map(user.get(0),DTOUser.class);
			
		}
		
		
		return usuario;
		
		
	}
	
	
	


}
