package com.bofugroup.springboot.backend.apirest.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.bofugroup.springboot.backend.apirest.models.entity.User;

public interface IUserDAO extends CrudRepository<User,Long>{
	
	
	//@Query(value = "SELECT * FROM hire_user WHERE email =? useremail",nativeQuery = true)
	

	public List<User> findByEmail(String email);
	

}
