package com.bofugroup.springboot.backend.apirest.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bofugroup.springboot.backend.apirest.models.dto.DTOClient;
import com.bofugroup.springboot.backend.apirest.models.entity.Client;
import com.bofugroup.springboot.backend.apirest.models.services.ClienteService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = {"http://localhost:4200"})
public class ClienteRestController {
	
	
	@Autowired
	private ClienteService clienteService;
	
	

	// Se obtienen todos los clientes por id de usuario
	@GetMapping("/clientes/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		
		List<DTOClient> clientes = null;
		Map <String, Object> response = new HashMap<>();
		
		try {
			clientes = clienteService.findAll(id);
		}catch(DataAccessException e){
			response.put("mensaje", "Huvo un error en el momento de realizar la consulta, intente de nuevo");
			return new ResponseEntity<Map <String, Object>>(response,HttpStatus.CONFLICT);		
		}
		
		if(clientes.size()==0) {
			response.put("mensaje", "El id ".concat(id.toString()).concat(" no se ha encontrado en la base de datos"));
			return new ResponseEntity<Map <String, Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		response.put("messeage","Successfull request clients");
		response.put("clientes", clientes);
		
		return new ResponseEntity<Map <String, Object>>(response, HttpStatus.OK);
	}
	
	// Se guarda el cliente con su respéctivo usuario y Sector
	
	@PostMapping("/clientes")
	public ResponseEntity<?> create(@RequestBody DTOClient client) {
		
		
		
		DTOClient clientenew=null;
		
		Map<String, Object> response = new HashMap<>();
		
		
		
		if(client==null) {
			response.put("message","Object is null");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			
			clientenew = clienteService.save(client);
			
		}catch(DataAccessException e) {
			
			response.put("mensaje","Ocurrio un error al insertar el cliente en la base de datos");
			
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		response.put("mensaje", "El cliente ha sido ceado con exito");
		response.put("cliente", clientenew);
		
		 return new ResponseEntity<Map<String, Object>>(response,HttpStatus.CREATED);
	}
	
	


}
